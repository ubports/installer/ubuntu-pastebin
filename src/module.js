"use strict";

/*
 * Copyright (C) 2020 UBports Foundation <info@ubports.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import axios from "axios";
import FormData from "form-data";

/**
 * Paste content to paste.ubuntu.com. Please be careful not to overload the site with useless data! Paste.ubuntu.com is intended for use as a short-term exchange of pasted information between parties. All submitted data is considered public information. Submitted data is not guaranteed to be permanent, and may be removed at any time. Please do not set up programs to frequently send data to the site!
 * @async
 * @param {Promise<String>} content - content to paste
 * @param {String} [poster] - user name
 * @param {String} [syntax] - syntax for highlighting
 * @param {String} [expiration] - how long to store the log
 * @returns {String} paste url
 * @throws if paste failed
 */
export async function paste(
  content,
  poster = "Ubuntu User",
  syntax = "text",
  expiration = "month"
) {
  if (!content) {
    throw new Error("no content provided");
  } else if (!["day", "week", "month", "year"].includes(expiration)) {
    throw new Error(`Invalid expiration option: ${expiration}`);
  }
  const form = new FormData();
  form.append("poster", poster);
  form.append("syntax", syntax);
  form.append("expiration", expiration);
  form.append("content", await content);
  console.warn(
    "Since Ubuntu Pastebin now requires a login, it is no longer possible to send anonymous paste data there. This package will no longer be maintained."
  );

  return axios
    .post("http://paste.ubuntu.com", form, { headers: form.getHeaders() })
    .then(r => {
      if (r.request.path && r.request.path.length >= 2) {
        return r.request.path;
      } else {
        throw new Error(
          `Failed to paste: Invalid path response: ${r.request.path}`
        );
      }
    })
    .then(pasteId => `https://paste.ubuntu.com${pasteId}`)
    .catch(error => {
      throw new Error(`Failed to paste: ${error}`);
    });
}
