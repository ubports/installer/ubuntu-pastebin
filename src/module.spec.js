// const { OpenCutsReporter } = require("../lib/module.cjs");
import { jest } from "@jest/globals";

import axios from "axios";

import { paste } from "./module.js";

describe("paste()", () => {
  it("should resolve url", () => {
    axios.post = jest.fn().mockResolvedValue({
      request: {
        path: "/ab"
      }
    });
    expect(paste("content")).resolves.toEqual("https://paste.ubuntu.com/ab");
  });
  it("should reject on invalid response", () => {
    axios.post = jest.fn().mockResolvedValue({ request: {} });
    expect(paste("content")).rejects.toThrow(
      "Failed to paste: Error: Failed to paste: Invalid path response: undefined"
    );
  });
  it("should reject on error", () => {
    axios.post = jest.fn().mockRejectedValue("paste error");
    expect(paste("a")).rejects.toThrow("Failed to paste: paste error");
  });
  it("should reject on empty content", () => {
    axios.post = jest.fn();
    expect(paste()).rejects.toThrow("no content provided");
  });
  it("should reject on invalid expiration", () => {
    axios.post = jest.fn();
    expect(paste("content", "me", "text", "aeon")).rejects.toThrow(
      "Invalid expiration option: aeon"
    );
  });
});
