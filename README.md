# ubuntu-pastebin

[![ci](https://gitlab.com/ubports/installer/ubuntu-pastebin/badges/master/pipeline.svg)](https://gitlab.com/ubports/installer/ubuntu-pastebin/) [![coverage report](https://gitlab.com/ubports/installer/ubuntu-pastebin/badges/master/coverage.svg)](https://ubports.gitlab.io/installer/ubuntu-pastebin/coverage/) [![npm](https://img.shields.io/npm/v/ubuntu-pastebin)](https://www.npmjs.com/package/ubuntu-pastebin)

DEPRECATED: Since [Ubuntu Pastebin](https://paste.ubuntu.com/) now requires a login, it is no longer possible to send anonymous paste data there. This package will no longer be maintained.

A tiny npm package that allows you to paste data to [Ubuntu Pastebin](https://paste.ubuntu.com/). Please be careful not to overload the site with useless data! Paste.ubuntu.com is intended for use as a short-term exchange of pasted information between parties. All submitted data is considered public information. Submitted data is not guaranteed to be permanent, and may be removed at any time. **Please do not set up programs to frequently send large amounts of data to the site**!

## Usage

Install the library by running `npm i ubuntu-pastebin`.

```javascript
import {paste} from "ubuntu-pastebin";
paste("this is some content", "my awesome username", "text", "week")
  .then(url => console.log(`paste available at: ${url}`));
```

## Development

```bash
$ npm install # to install dependencies
$ npm update # to update dependencies
$ npm audit fix # to install security patches
$ npm run lint # to check coding style
$ npm run lint-fix # to automatically fix coding style issues
$ npm run test # to run unit-tests with coverage reports
$ npm run docs # to build detailed jsdoc documentation
$ npm run build # to compile backwards-compatible code using rollup
```

## History

The library was originally developed for use in the [UBports Installer](https://github.com/ubports/ubports-installer), but it might be useful to other projects. Semantic versioning will ensure API stability for public functions.

## License

Original development by [Jan Sprinz](https://spri.nz). Copyright (C) 2020 [UBports Foundation](https://ubports.com).

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
