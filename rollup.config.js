export default {
  input: 'src/module.js',
  output: {
    file: 'lib/module.cjs',
    format: 'cjs'
  },
  external: ["axios", "form-data"]
};
